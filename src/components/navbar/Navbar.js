import "./navbar.scss";
import LoginButton from "../login/LoginLogout";

export default function Navbar(){
    return(
        
        <div className="navbar">
            
        <div className="navigation">
            <div className="items">
                Accueil
            </div>
            <div className="items">
                Festivals
            </div>
            <div className="items">
                Artistes
            </div>
            <div className="items">
                Messagerie
            </div>
            <div className="items">
                Profil
            </div>
        </div>
        <div className="connexion">
            <div className="log">
                <LoginButton/>
            </div>
        </div>            
        </div>

    )
}