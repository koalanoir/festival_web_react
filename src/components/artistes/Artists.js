import Footer from "../footer/Footer";
import Navbar from "../navbar/Navbar";
import React, { useEffect, useState } from "react";
import axios from'axios';
function Artists() {
    const [artists,setArtist] =useState(undefined)

    useEffect(() => {
        let mounted=true;
        mounted&&axios.get('http://localhost:5000/artists')
        .then(res=>{
            mounted=false;
            setArtist(res.data)
        })
  
        return mounted = true;
    })
    return (
      <div className="App">
        <Navbar/>
        <img src=""></img>
        { artists && artists.map((artist,index) => <div key={index} className=" card_artist">{artist.name}<br/><img src={artist.image}/></div>)}
        <Footer/>
      </div>
    );
  }
  
  export default Artists;