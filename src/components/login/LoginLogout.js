import React, { useEffect, useState } from "react";
import { useAuth0 } from "@auth0/auth0-react";
import "./LoginLogout.scss";

const LoginButton = () => {
  const { getAccessTokenSilently, logout , loginWithRedirect, user, isAuthenticated } = useAuth0();
  const [userMetadata, setUserMetadata] = useState(null);

  useEffect(() => {
    const getUserMetadata = async () => {
      const domain = "dev-cjxhb-x9.eu.auth0.com";
  
      try {
        const accessToken = await getAccessTokenSilently({
          audience: `https://${domain}/api/v2/`,
          scope: "read:current_user",
        });
  
        const userDetailsByIdUrl = `https://${domain}/api/v2/users/${user.sub}`;
  
        const metadataResponse = await fetch(userDetailsByIdUrl, {
          headers: {
            Authorization: `Bearer ${accessToken}`,
          },
        });
  
        const { user_metadata } = await metadataResponse.json();
  
        setUserMetadata(user_metadata);
      } catch (e) {
        console.log(e.message);
      }
    };
  
    getUserMetadata();
}, [getAccessTokenSilently, user?.sub]);

  if (isAuthenticated){
    return (
        <>
            <div className="img">
              <img src={user.picture} alt={user.name} />
            </div>
            <div className="img" >
              <label>{user.name}</label>
            </div>
            <div className="btn">
              <button onClick={() => logout()}>Se deconnecter</button>
            </div>
        </>

      );
  }

  return (
    <div className="login">
      <button onClick={() => loginWithRedirect()}>Se connecter</button>
    </div>
  );
};

export default LoginButton;