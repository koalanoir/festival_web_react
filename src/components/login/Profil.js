import React from "react";
import { useAuth0 } from "@auth0/auth0-react";

const UserInfo = () => {
    const {user,logout} = useAuth0();
    return (
        <>
            <img src={user.picture} alt={user.name} />
            <h2>{user.name}</h2>
            <p>{user.email}</p>
            <button onClick={() => logout()}>Se deconnecter</button>
        </>
      );
};
export default UserInfo;
