import './App.css';
import ROUTES, { RenderRoutes } from './routing/Routes';
function App() {
  return (
    <RenderRoutes routes={ROUTES}/>
  );
}

export default App;
