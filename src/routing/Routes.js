import React from "react";
import { Route, Routes, useLocation } from "react-router-dom";
import Artists from "../components/artistes/Artists";

//Scènes
import  Home  from "../components/home/home";
import Profil from "../components/profil/profil";

const ROUTES = [
    {
        path: "/",
        key: "ROOT",
        exact: true,
        component: <Home />,
    },
    {
        path: "/artist",
        key: "ARTIST",
        exact: true,
        component:<Artists/>
    },
    {
        path: "/profil",
        key: "PROFIL",
        exact: true,
        component: <Profil/>
    }
];
export default ROUTES;

export function RenderRoutes({ routes }) {
    let location = useLocation();
    return (
        <Routes location={location}>
            {routes.map((route, i) => {
                return (
                    <Route
                        key={route.key}
                        path={route.path}
                        exact={route.exact}
                        element={route.component}
                        render={(props) => <route.component {...props} routes={route.routes} />}
                    />
                );
            })}
        </Routes>
    );
}